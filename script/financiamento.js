$('.campoCPF').mask('000.000.000-00', {reverse: true});
$('.campoData').mask('00/00/0000');
$('.campoCEP').mask('00000-000');
$('.campoTelefone').mask('(00) 0000-0000');

var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};
$('.campoCelular').mask(SPMaskBehavior, spOptions);
$('.campoDinheiro').mask('#.##0,00', {reverse: true});
$('.campoContaBancaria').mask('0000/00000-0', {reverse: false});
$('.campoRG').mask('AA-00.000.000', {reverse: false});
