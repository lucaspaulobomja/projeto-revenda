$(".img-conteudo").click(function() {
    var $this = $(this).attr("src");
    $("#imgPrincipal").attr("src", $this);
});

var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};
$('.campoTelefone').mask(SPMaskBehavior, spOptions);